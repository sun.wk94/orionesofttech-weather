'use client'
import { useRef, useState } from "react";
import Tile from "@/components/tile";
import { Toaster, toast } from "react-hot-toast";
import { apikey, baseUrl, weatherEndPoint } from "@/shared/constant";

interface ITileCount {
  id: string;
  name?: string;
  lat?: number;
  lon?: number;
  interval: number;
}

export default function Home() {
  const inputRef = useRef(null);
  const refreshTimer = 50000;
  const [ weatherList, setWeatherList ] = useState<ITileCount[]>([
    { id: '5128581', interval: refreshTimer, name: 'New York'}, 
    { id: '2643743', interval: refreshTimer, name: 'London'}, 
    { id: '2950159', interval: refreshTimer, name: 'Berlin'}
  ]);

  function isExisting(value: ITileCount): boolean {
    return weatherList.indexOf(value) > -1;
  }

  async function handleAddTile(id: ITileCount) {
    setWeatherList((oldArray) => [
      ...oldArray,
      id,
    ]);
	}

  const handleRemoveTile = value => {
    setWeatherList(oldArray => {
      return oldArray.filter(name => name.id !== value)
    })
  }  

  async function searchCity(city: string) {
    if (city) {
      try {
        const response = await fetch(`${baseUrl}${weatherEndPoint}?q=${city}&appid=${apikey}&units=metric`);
        if (response.status > 400) {
          toast.error('No record');
          return
        }
        await response.json().then(result => {
          if (!isExisting(result.id)) {
            handleAddTile({id: result.id, interval: refreshTimer}).then(() => {
              toast.success('Added successful');
            })        
          } else {
            toast.error('Duplicated record');
          }      
        })
      } catch (error) {
        toast.error('No record');
      }
    }    
  }

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className=" w-2/4">
        <div className="mb-6">
          <input ref={inputRef} name="message" placeholder="Please insert your favourite city" required
            className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500 bg-gray-700 text-white"></input>
        </div>
        <button type="button" onClick={() => searchCity(inputRef.current.value)}
          className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:shadow-outline-blue">
          Add City
        </button>
      </div>
      <div className="flex flex-row flex-wrap mt-4">
        {
          weatherList.map((tile, index) => (
            <Tile
              key={index}
              id={tile.id}
              interval={tile.interval}
              onRemove={() => handleRemoveTile(tile.id)}
            />
          ))
        }        
      </div>
      <Toaster position="bottom-center" />
    </main>
  );
}

'use client'
import { apikey, baseUrl, weatherEndPoint } from '@/shared/constant';
import Image from 'next/image';
import React, { useEffect, useState } from 'react'

export interface ITile {
  id: string;
  name: string,
  temperature: string,
  situation: string,
  forecast: string,
  images: string
}

interface IProp {
  id: string;
  interval: number;
  onRemove: any;
  lat?: number;
  lon?: number;
}

function Tile(props: IProp) {  
  const [ weatherData, setWeatherData ] = useState<ITile>();

  useEffect(() => {    
    const fetchWeather = async () => {      
      const response = await fetch(`${baseUrl}${weatherEndPoint}?id=${props.id}&appid=${apikey}&units=metric`);
      await response.json().then(result => {
        let newValue: ITile = {
          id: props.id,
          name: result.name,
          temperature: result.main.temp,
          situation: result.weather[0].main,
          forecast: '',
          images: `${baseUrl}/img/w/${result.weather[0].icon}.png`,
        };
        setWeatherData(newValue);
    })}
    fetchWeather()
    const intervalId = setInterval(fetchWeather, props.interval);

    return () => clearInterval(intervalId);

  }, [props.id, props.interval])

  return (
    weatherData && <div key={`tile-${props.id}`} className="rounded overflow-hidden shadow-lg cursor-pointer my-2 hover:opacity-65" onClick={() => props.onRemove(weatherData.id)}>
      <Image className="w-full" src={weatherData.images} width={150} height={150} alt="Sunset in the mountains" />
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{weatherData.name}</div>
      </div>
      <div className="px-6 pt-4 pb-2">
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{weatherData.situation}</span>
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{weatherData.temperature}C</span>
      </div>
    </div>      
  )
}

export default Tile
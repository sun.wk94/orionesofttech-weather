export const baseUrl = 'https://api.openweathermap.org';

// This value should replacing in .env file,
// but because of this project is test assessment so it will place in constant file
// API key will be revoke after 2024 June.
export const apikey = '729709b0607c57b0a37b22c1b6ee506d';

// Endpoint
export const weatherEndPoint = '/data/2.5/weather';
export const forecastEndPoint = '/data/2.5/forecast';